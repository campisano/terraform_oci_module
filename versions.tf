terraform {
  required_version = ">= 0.13"

  required_providers {
    oci = "5.10.0"
  }
}
