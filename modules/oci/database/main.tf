resource "oci_database_autonomous_database" "database" {
  compartment_id = var.compartment_id
  display_name   = var.display_name
  db_name        = var.db_name

  admin_password = var.password

  db_workload = "OLTP"
  db_version  = "19c"

  is_free_tier             = true
  license_model            = "LICENSE_INCLUDED"
  cpu_core_count           = 1
  data_storage_size_in_tbs = 1

  is_mtls_connection_required = false
  whitelisted_ips             = var.whitelisted_ips
}
