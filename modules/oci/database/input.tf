variable "compartment_id"    { type = string }
variable "db_name"           { type = string }
variable "display_name"      { type = string }
variable "whitelisted_ips"   { type = list(string) }
variable "password"          { type = string }
