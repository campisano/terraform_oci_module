resource "oci_objectstorage_bucket" "bucket" {
  compartment_id = var.compartment_id
  name           = var.name
  namespace      = var.namespace
  access_type    = var.access_type
  versioning     = var.versioning
}
