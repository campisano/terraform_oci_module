variable "name"           { type = string }
variable "compartment_id" { type = string }
variable "namespace"      { type = string }
variable "access_type"    { type = string }
variable "versioning"     { type = string }
