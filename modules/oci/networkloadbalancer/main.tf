resource "oci_network_load_balancer_network_load_balancer" "balancer" {
  compartment_id                 = var.compartment_id
  display_name                   = var.name
  subnet_id                      = var.subnet_id
  is_private                     = false
  is_preserve_source_destination = false
}
