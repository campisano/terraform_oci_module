variable "name"           { type = string }
variable "compartment_id" { type = string }
variable "subnet_id"      { type = string }
variable "is_private"     { type = bool }
