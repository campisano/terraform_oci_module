variable "name"              { type = string }
variable "balancer_id"       { type = string }
variable "listener_protocol" { type = string }
variable "listener_port"     { type = number }
variable "healthcheck"       { type = any }
variable "backend_port"      { type = number }
variable "backend_instances" { type = map(any) }
