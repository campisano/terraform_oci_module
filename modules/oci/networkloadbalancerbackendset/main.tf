resource "oci_network_load_balancer_backend_set" "backend_set" {
  name                     = "${var.name}-bset"
  network_load_balancer_id = var.balancer_id
  policy                   = "FIVE_TUPLE"
  is_preserve_source       = false

  health_checker {
    protocol           = var.healthcheck.protocol
    port               = var.healthcheck.port
    url_path           = lookup(var.healthcheck, "path"    , null)
    return_code        = lookup(var.healthcheck, "code"    , null)
    interval_in_millis = lookup(var.healthcheck, "interval", null)
    timeout_in_millis  = lookup(var.healthcheck, "timeout" , null)
    retries            = lookup(var.healthcheck, "retries" , null)
  }
}

resource "oci_network_load_balancer_listener" "listener" {
  name                     = "${var.name}-listener"
  default_backend_set_name = oci_network_load_balancer_backend_set.backend_set.name
  network_load_balancer_id = var.balancer_id
  protocol                 = var.listener_protocol
  port                     = var.listener_port
}

resource "oci_network_load_balancer_backend" "backend" {
  for_each = var.backend_instances

  name                     = "${var.name}-bakend-${each.key}"
  backend_set_name         = oci_network_load_balancer_backend_set.backend_set.name
  network_load_balancer_id = var.balancer_id
  port                     = var.backend_port
  target_id                = each.value.id
}
