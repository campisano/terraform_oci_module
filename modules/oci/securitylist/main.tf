resource "oci_core_security_list" "security_list" {
  compartment_id = var.compartment_id
  display_name   = var.name
  vcn_id         = var.vcn.id

  ingress_security_rules {
    protocol = var.iana_protocol_number
    source = var.cidr

    dynamic tcp_options {
      for_each = var.iana_protocol_number == "6" ? toset([1]) : toset([])
      content {
        min = contains(keys(var.ports), "min") ? var.ports.min : null
        max = contains(keys(var.ports), "max") ? var.ports.max : null
      }
    }

    dynamic udp_options {
      for_each = var.iana_protocol_number == "17" ? toset([1]) : toset([])
      content {
        min = contains(keys(var.ports), "min") ? var.ports.min : null
        max = contains(keys(var.ports), "max") ? var.ports.max : null
      }
    }
  }
}
