variable "name"                 { type = string }
variable "compartment_id"       { type = string }
variable "vcn"                  { type = any }
variable "iana_protocol_number" { type = string }
variable "cidr"                 { type = string }
variable "ports"                { type = any }
