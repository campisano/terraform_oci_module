#!/usr/bin/env bash

# please note, all variables not passed by terraform must be escaped with a double dollar char (e.g. $$VARNAME)
## NOTE: swap must be disabled permanently

set -x -o errexit -o nounset -o pipefail

(
    echo -e "\n\t\tCustom init script start...\n"

    # converting template vars in env vars
    export NET_ADDR=${NET_ADDR}
    export INSTANCE_SHAPE=${INSTANCE_SHAPE}

    if test -z "$${NET_ADDR}"
    then
        echo >&2 "Error: missing NET_ADDR env var"
        exit 1
    fi

    if test "$${INSTANCE_SHAPE}" = "VM.Standard.E2.1.Micro"
    then
        export ARCH="amd64"
    elif test "$${INSTANCE_SHAPE}" = "VM.Standard.A1.Flex"
    then
        export ARCH="arm64"
    else
        echo >&2 "Error: invalid INSTANCE_SHAPE env var"
        exit 1
    fi



    echo -e "\n\tconfigure apt and upgrade:"
    echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
    apt-get -y update > /dev/null
    apt-get -y install --no-install-recommends apt-utils > /dev/null
    apt-get -y install ca-certificates > /dev/null
    apt-get -y autoremove --purge ufw snapd unattended-upgrades networkd-dispatcher open-iscsi modemmanager > /dev/null
    apt-get -y dist-upgrade > /dev/null
    apt-get -y install binutils nano psmisc lsof findutils grep less tar gzip bzip2 procps iptables kmod curl cron dnsutils telnet iputils-ping netcat-traditional > /dev/null
    apt-get -y clean > /dev/null

    echo -e "\n\tremove firewall rules:"
    iptables-save > /root/iptables.bkp
    iptables -P INPUT ACCEPT
    iptables -P FORWARD ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -t nat -F
    iptables -t mangle -F
    iptables -F
    iptables -X
    ip6tables -P INPUT ACCEPT
    ip6tables -P FORWARD ACCEPT
    ip6tables -P OUTPUT ACCEPT
    ip6tables -t nat -F
    ip6tables -t mangle -F
    ip6tables -F
    ip6tables -X
    echo -n > /etc/iptables/rules.v4



    echo -e "\n\tinstall kubernetes:"
    # deps
    apt-get -y install socat conntrack > /dev/null
    #
    # from https://kubernetes.io/docs/setup/production-environment/container-runtimes/
    #
    # OS config
    cat > /etc/modules-load.d/k8s.conf << 'EOF'
overlay
br_netfilter
EOF
    modprobe overlay
    modprobe br_netfilter
    cat > /etc/sysctl.d/k8s.conf << 'EOF'
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
    sysctl -w net.bridge.bridge-nf-call-iptables=1
    sysctl -w net.bridge.bridge-nf-call-ip6tables=1
    sysctl -w net.ipv4.ip_forward=1
    #
    # containerd
    VERSION="1.7.3"
    curl -fsSL "https://github.com/containerd/containerd/releases/download/v$${VERSION}/containerd-$${VERSION}-linux-$${ARCH}.tar.gz" | tar -C /usr/local/ -xz
    mkdir -p /etc/containerd
    containerd config default > /etc/containerd/config.toml
    sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
    # runc
    VERSION="1.1.8"
    curl -fsSL -o /usr/local/sbin/runc "https://github.com/opencontainers/runc/releases/download/v$${VERSION}/runc.$${ARCH}"
    chmod 0755 /usr/local/sbin/runc
    # cni
    VERSION="1.3.0"
    mkdir -p /opt/cni/bin
    curl -fsSL "https://github.com/containernetworking/plugins/releases/download/v$${VERSION}/cni-plugins-linux-$${ARCH}-v$${VERSION}.tgz" | tar -C /opt/cni/bin -xz
    # containerd service
    mkdir -p /usr/local/lib/systemd/system/
    curl -fsSL -o /usr/local/lib/systemd/system/containerd.service "https://raw.githubusercontent.com/containerd/containerd/main/containerd.service"
    mkdir -p /etc/cni/net.d
    systemctl daemon-reload
    systemctl enable containerd
    systemctl start containerd
    #
    # kubeadm install
    #
    mkdir -p /usr/local/bin
    cd /usr/local/bin
    # crictl
    VERSION="1.27.0"
    curl -fsSL "https://github.com/kubernetes-sigs/cri-tools/releases/download/v$${VERSION}/crictl-v$${VERSION}-linux-$${ARCH}.tar.gz" | tar -xz
    chmod 0755 crictl
    chown root:root crictl
    # kubeadm / kubelet
    VERSION="1.27.4"
    curl -fsSL --remote-name-all "https://dl.k8s.io/release/v$${VERSION}/bin/linux/$${ARCH}/{kubeadm,kubelet}"
    chmod 0755 {kubeadm,kubelet}
    chown root:root {kubeadm,kubelet}
    # kubernetes services
    VERSION="0.15.1"
    curl -fsSL "https://raw.githubusercontent.com/kubernetes/release/v$${VERSION}/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service" | sed "s:/usr/bin:/usr/local/bin:g" > /etc/systemd/system/kubelet.service
    mkdir -p /etc/systemd/system/kubelet.service.d
    curl -fsSL "https://raw.githubusercontent.com/kubernetes/release/v$${VERSION}/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf" | sed "s:/usr/bin:/usr/local/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
    # kubectl
    VERSION="1.27.4"
    curl -fsSL -O "https://dl.k8s.io/release/v$${VERSION}/bin/linux/$${ARCH}/kubectl"
    chmod 0755 kubectl
    chown root:root kubectl



    echo -e "\n\t\tCustom init script completed. Reboot in 1 minutes...\n"
    shutdown -r +1
) &> /root/init_script.log
