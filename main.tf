data "oci_identity_availability_domain" "ad" {
  compartment_id = var.oci_provider.tenancy_ocid
  ad_number      = 1
}

data "oci_objectstorage_namespace" "namespace" {
  compartment_id = var.oci_provider.tenancy_ocid
}

resource "oci_identity_compartment" "compartment" {
  compartment_id = var.oci_provider.tenancy_ocid
  name           = "tf-compartment"
  description    = "compartment created by terraform"
  enable_delete  = true
}

module "oci_vcn" {
  source = "./modules/oci/vcn"

  for_each = var.oci_vcn_module

  name           = each.key
  compartment_id = oci_identity_compartment.compartment.id
  cidr_block     = each.value.cidr_block
}

module "oci_securitylist" {
  source = "./modules/oci/securitylist"

  for_each = var.oci_securitylist_module

  name                 = each.key
  compartment_id       = oci_identity_compartment.compartment.id
  vcn                  = module.oci_vcn[each.value.vcn_name].vcn
  iana_protocol_number = each.value.iana_protocol_number
  cidr                 = each.value.cidr
  ports                = lookup(each.value, "ports", null)
}

module "oci_subnet" {
  source = "./modules/oci/subnet"

  for_each = var.oci_subnet_module

  name              = each.key
  compartment_id    = oci_identity_compartment.compartment.id
  vcn               = module.oci_vcn[each.value.vcn_name].vcn
  ad_name           = data.oci_identity_availability_domain.ad.name
  cidr_block        = each.value.cidr_block
  security_list_ids = [for sl_name in each.value.securitylists: module.oci_securitylist[sl_name].securitylist.id]
}

module "oci_instance" {
  source = "./modules/oci/instance"

  for_each = var.oci_instance_module

  name             = each.key
  compartment_id   = oci_identity_compartment.compartment.id
  subnet           = module.oci_subnet[each.value.subnet_name].subnet
  ad_name          = data.oci_identity_availability_domain.ad.name
  keypair_path     = each.value.keypair_path
  instance_shape   = each.value.instance_shape
  shape_ocpus      = lookup(each.value, "shape_ocpus", null)
  shape_mem        = lookup(each.value, "shape_mem", null)
  image_ocid       = each.value.image_ocid
  boot_disk_size   = lookup(each.value, "boot_disk_size", null)
  static_ip        = lookup(each.value, "static_ip", false)
  init_script_path = lookup(each.value, "init_script_path", null)
}

module "oci_networkloadbalancer" {
  source = "./modules/oci/networkloadbalancer"

  for_each = var.oci_networkloadbalancer_module

  name           = each.key
  compartment_id = oci_identity_compartment.compartment.id
  subnet_id      = module.oci_subnet[each.value.subnet_name].subnet.id
  is_private     = each.value.is_private
}

module "oci_networkloadbalancerbackendset" {
  source = "./modules/oci/networkloadbalancerbackendset"

  for_each = var.oci_networkloadbalancerbackendset_module.backend_sets

  name              = "${var.oci_networkloadbalancerbackendset_module.lb_name}-${each.key}"
  balancer_id       = module.oci_networkloadbalancer[var.oci_networkloadbalancerbackendset_module.lb_name].networkloadbalancer.id
  listener_protocol = each.value.listener_protocol
  listener_port     = each.value.listener_port
  healthcheck       = each.value.healthcheck
  backend_port      = each.value.backend_port
  backend_instances = {for i_name in each.value.backend_instances: i_name => module.oci_instance[i_name].instance}
}

module "oci_database" {
  source = "./modules/oci/database"

  for_each = var.oci_database_module

  display_name    = each.key
  compartment_id  = oci_identity_compartment.compartment.id
  db_name         = each.value.db_name
  whitelisted_ips = [for i_name in each.value.allow_instances: module.oci_instance[i_name].instance.public_ip]
  password        = "YOUR_PASSWORD!!!"
}

module "oci_objectstorage" {
  source = "./modules/oci/objectstorage"

  for_each = var.oci_objectstorage_module

  name            = each.key
  compartment_id  = oci_identity_compartment.compartment.id
  namespace       = data.oci_objectstorage_namespace.namespace.namespace
  access_type     = each.value.access_type
  versioning      = each.value.versioning
}
