#!/usr/bin/env bash

# please note, all variables not passed by terraform must be escaped with a double dollar char (e.g. $$VARNAME)
## NOTE: swap must be disabled permanently

set -x -o errexit -o nounset -o pipefail

(
    echo -e "\n\t\tCustom init script start...\n"

    # converting template vars in env vars
    export NET_ADDR=${NET_ADDR}
    export INSTANCE_SHAPE=${INSTANCE_SHAPE}

    if test -z "$${NET_ADDR}"
    then
        echo >&2 "Error: missing NET_ADDR env var"
        exit 1
    fi

    if test "$${INSTANCE_SHAPE}" = "VM.Standard.E2.1.Micro"
    then
        export ARCH="amd64"
    elif test "$${INSTANCE_SHAPE}" = "VM.Standard.A1.Flex"
    then
        export ARCH="arm64"
    else
        echo >&2 "Error: invalid INSTANCE_SHAPE env var"
        exit 1
    fi



    echo -e "\n\tconfigure apt and upgrade:"
    echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
    apt-get -y update > /dev/null
    apt-get -y install --no-install-recommends apt-utils > /dev/null
    apt-get -y install ca-certificates > /dev/null
    apt-get -y autoremove --purge ufw snapd unattended-upgrades networkd-dispatcher open-iscsi modemmanager > /dev/null
    apt-get -y dist-upgrade > /dev/null
    apt-get -y install binutils nano psmisc lsof findutils grep less tar gzip bzip2 procps iptables kmod curl cron dnsutils telnet iputils-ping netcat-traditional > /dev/null
    apt-get -y clean > /dev/null

    echo -e "\n\tconfigure firewall:"
    iptables-save > /root/iptables.bkp
    iptables --insert INPUT 1 --source "$${NET_ADDR}" --proto tcp --jump ACCEPT
    iptables --insert INPUT 1 --source "$${NET_ADDR}" --proto udp --jump ACCEPT
    iptables-save > /etc/iptables/rules.v4

   echo -e "\n\tinstall and setup docker:"
   apt-get -y install docker.io docker-compose > /dev/null
   apt-get -y clean > /dev/null
   mkdir -p /srv/local/swarm
   useradd --system --uid 500 --gid nogroup --groups docker --no-create-home --home-dir /srv/local/swarm --shell /bin/bash swarm
   chown swarm:nogroup /srv/local/swarm

    echo -e "\n\t\tCustom init script completed. Reboot in 1 minutes...\n"
    shutdown -r +1
) &> /root/init_script.log
